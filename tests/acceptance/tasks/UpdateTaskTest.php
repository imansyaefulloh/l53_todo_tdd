<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UpdateTaskTest extends TestCase
{
    use DatabaseTransactions;
    
    /** @test */
    public function user_can_click_edit_button_to_view_edit_page()
    {
        list($user, $group, $task) = $this->createTaskWithGroupAndUser();

        $this->actingAs($user)
            ->visit('/groups/' . $group->id)
            ->click('update-task-' . $task->id);

        $this->seePageIs('/groups/' . $group->id . '/tasks/' . $task->id . '/edit')
            ->seeInField('task_title', $task->title)
            ->see('Editing task: ' . $task->title);
    }

    /** @test */
    public function user_can_change_task_title()
    {
        list($user, $group, $task) = $this->createTaskWithGroupAndUser();

        $this->actingAs($user)
            ->visit('/groups/' . $group->id . '/tasks/' . $task->id . '/edit')
            ->type('Updated task title', 'task_title')
            ->press('Save task');

        $this->seePageIs('/groups/'.$group->id)
            ->see('Task was updated')
            ->see('Updated task title')
            ->seeInDatabase('tasks', [
                'title' => 'Updated task title'
            ]);
    }

    /** @test */
    public function user_cannot_update_task_with_empty_title()
    {
        list($user, $group, $task) = $this->createTaskWithGroupAndUser();

        $this->actingAs($user)
            ->visit('/groups/' . $group->id . '/tasks/' . $task->id . '/edit')
            ->type('', 'task_title')
            ->press('Save task');

        $this->seePageIs('/groups/' . $group->id . '/tasks/' . $task->id . '/edit')
            ->see('Task title is required')
            ->dontSeeInDatabase('tasks', [
                'title' => ''
            ]);
    }

    /** @test */
    public function user_cannot_update_another_user_task()
    {
        $user = factory(App\User::class)->create();

        $anotherUser = factory(App\User::class)->create();

        $inaccessilbleGroup = factory(App\Group::class)->create(['user_id' => $anotherUser->id]);

        $task = factory(App\Task::class)->create(['group_id' => $inaccessilbleGroup->id]);

        $this->actingAs($user)->call('patch', '/groups/' . $inaccessilbleGroup->id . '/tasks/' . $task->id, [
            'task_title' => 'Updated task title'
        ]);

        $this->assertResponseStatus(403);
    }

    /** @test */
    public function user_cannot_see_update_page_for_another_user_task()
    {
        $user = factory(App\User::class)->create();

        $anotherUser = factory(App\User::class)->create();

        $inaccessilbleGroup = factory(App\Group::class)->create(['user_id' => $anotherUser->id]);

        $task = factory(App\Task::class)->create(['group_id' => $inaccessilbleGroup->id]);

        $this->actingAs($user)
            ->call('get', '/groups/' . $inaccessilbleGroup->id . '/tasks/' . $task->id . '/edit');

        $this->assertResponseStatus(403);   
    }
}




















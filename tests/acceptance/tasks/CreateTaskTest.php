<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreateTaskTest extends TestCase
{
    use DatabaseTransactions;
    
    /** @test */
    public function user_can_create_task()
    {
        $user = factory(App\User::class)->create();
        $group = factory(App\Group::class)->create(['user_id' => $user->id]);

        $this->actingAs($user)
            ->visit('/groups/' . $group->id)
            ->type('Buy milk', 'task_title')
            ->press('create-task');

        $this->see('Task created successfully')
            ->see('Buy milk')
            ->seeInDatabase('tasks', [
                'title' => 'Buy milk'
            ]);
    }

    /** @test */
    public function user_cannot_create_task_with_empty_title()
    {
        $user = factory(App\User::class)->create();
        $group = factory(App\Group::class)->create(['user_id' => $user->id]);

        $this->actingAs($user)
            ->visit('/groups/' . $group->id)
            ->type('', 'task_title')
            ->press('create-task');

        $this->see('Please enter a task title')
            ->dontSeeInDatabase('tasks', [
                'title' => ''
            ]);
    }

    /** @test */
    public function user_cannot_create_task_in_another_user_group()
    {
        $user = factory(App\User::class)->create();

        $anotherUser = factory(App\User::class)->create();

        $inaccessilbleGroup = factory(App\Group::class)->create(['user_id' => $anotherUser->id]);

        $this->actingAs($user)
            ->post('/groups/' . $inaccessilbleGroup->id . '/tasks', [
                'task_title' => 'Buy soap'
            ]);

        $this->assertResponseStatus(403);
    }    
}




















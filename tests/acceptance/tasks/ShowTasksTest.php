<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ShowTasksTest extends TestCase
{
    use DatabaseTransactions;
    
    /** @test */
    public function user_can_see_all_task_in_dashboard()
    {
        $user = factory(App\User::class)->create();
        $groups = factory(App\Group::class, 5)->create(['user_id' => $user->id]);

        $createdTask = collect();

        foreach ($groups as $group) {
            $createdTask->push(factory(App\Task::class)->create(['group_id' => $group->id]));
        }

        $this->actingAs($user)
            ->visit('/home/');

        $createdTask->each(function($task) {
            $this->see($task->title)
                ->see('(' . $task->group->title . ')');
        });
    }  
}




















<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreateGroupTest extends TestCase
{
    use DatabaseTransactions;
    
    /** @test */
    public function user_can_create_group()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->visit('/home')
            ->type('Shopping list', 'group_title')
            ->press('create-group');

        $this->see('Group created successfully')
            ->see('Shopping list')
            ->seeInDatabase('groups', [
                'title' => 'Shopping list'
            ]);
    }

    /** @test */
    public function user_cannot_create_group_with_empty_title()
    {
        $user = factory(App\User::class)->create();

        $this->actingAs($user)
            ->visit('/home')
            ->type('', 'group_title')
            ->press('create-group');

        $this->dontSee('Group created successfully')
            ->see('Please enter a group title')
            ->dontSeeInDatabase('groups', [ 
                'title' => ''
            ]);;
    }    
}




















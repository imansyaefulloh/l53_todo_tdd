<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function user_can_login()
    {
        $user = factory(App\User::class)->create(['email' => 'iman@testing.com']);

        $this->visit('/login')
            ->type('iman@testing.com', 'email')
            ->type('secret', 'password')
            ->press('Login');

        $this->seePageIs('/home');

        $this->assertEquals(Auth::user()->email, $user->email);
    }
}



















<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegisterTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function user_can_register_an_account()
    {
        $this->visit('/register')
            ->type('Iman Syaefulloh', 'name')
            ->type('iman@testing.com', 'email')
            ->type('secret', 'password')
            ->type('secret', 'password_confirmation')
            ->press('Register');

        $this->seePageIs('/home')
            ->seeInDatabase('users', [
                'name' => 'Iman Syaefulloh',
                'email' => 'iman@testing.com'
            ]);

    }
}



















@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Editing task: {{$task->title}}
                </div>
                <div class="panel-body">
                    <form action="{{ route('tasks.update', [$task->group, $task]) }}" method="post">
                        {{csrf_field()}}
                        {{method_field('PATCH')}}
                        <div class="form-group {{ $errors->has('task_title') ? 'has-error' : '' }}">
                            {{-- <div class="input-group"> --}}
                                <label for="task_title">Task title</label>
                                <input type="text" name="task_title" id="task_title" class="form-control" value="{{$task->title}}">
                                
                            {{-- </div> --}}
                            {!! $errors->first('task_title', '<p class="help-block">:message</p>') !!}
                        </div>
                        <span class="form-group">
                            <button class="btn btn-primary" type="submit" id="create-group">
                                <i class="fa fa-save"></i> Save task
                            </button>
                        </span>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection